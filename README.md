# iframe

Lokal html-fil för att kunna testa RCO iframe på lokal dator. jQuery som finns med i htmlfilen är ett krav för att
iframelösningen skall fungera "hela vägen".

* Öppna https://bitbucket.org/resursbankplugins/iframe-test/raw/master/index.html och kopiera innehållet.
* Klistra in innehållet i din egen lokala html-fil.
* Öppna filen i browsern.
  * Klistra in en iframe-url utan html-taggarna.
  ![Exempel 1](https://bitbucket.org/resursbankplugins/iframe-test/raw/master/Selection_784.png)
* Klicka på Load-knappen. Iframen skall nu laddas in:
  ![Exempel 2](https://bitbucket.org/resursbankplugins/iframe-test/raw/master/Selection_785.png)
